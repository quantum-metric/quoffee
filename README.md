- [How to Use for non-QM Teams](#how-to-use-for-non-qm-teams)
- [quoffee](#quoffee)
- [How does this work?](#how-does-this-work)
- [Installation](#installation)
  - [Build the Form](#build-the-form)
  - [Add the Script](#add-the-script)
  - [Send Invites](#send-invites)
  - [Unenroll](#unenroll)

# How to Use for non-QM Teams

This is a little script that has helped us out, especially during the social distancing time.

Anyone is welcome to use this free of charge with no expectation for support or follow up enhancements. It works for us and we hope it works for others too!

# quoffee

Randomly pair up people and send calendar invites. Helps remote teams get to know each other a little better.

As our company has grown, it's gotten harder to know people on a personal level. Throw in social distancing and that makes it even more difficult to know new hires or folks outside your immediate circle.

Quoffee will send you a weekly calendar invitation to meet with a random QMer for 20 minutes once a week. Get to know somebody new or reconnect with a familiar face! We hope that this is a fun, easy way to get to know your extended Q-Family.

# How does this work?

ANYONE with a QM email can sign up. ANY department. Sign up via G Forms and click the Submit button. That's it! Now you're enrolled in Quoffee and you'll be given a random pairing each week where you can connect on Google Meet.

(Once caveat, the initial calendar invite might conflict or be too early/late so you can move the event to a new, mutually-available time. Use Google Calendar's "Find a Time" feature to pick a better slot. Instructions here: https://youtu.be/h26iGzjCcJ8?t=25)

# Installation

## Build the Form

This Form will be your sign-up mechanism so that people can get paired up for the meetings.

1. Create a new Google Spreadsheet to hold the form answers.
2. From that Sheet, create a new Google Form.
3. Go to **Insert > Form**
4. Edit the Form to capture email addresses and include any other images or text that you want.

## Add the Script

This script will pair up people, and send the calendar invitations.

1. In your Sheet, click **Tools > Script Editor** to open the editor in a new tab.
1. Paste in the code found in this file: https://gitlab.com/quantum-metric/quoffee/-/blob/master/RandomMeetings.gs
1. Update any constants at the top to fit your needs. You’ll need to put in new value these variables to fit your needs: 
   1. CALENDAR_NAME
   2. MEETING_EVENT_SUMMARY_TITLE
   3. MEETING_DESCRIPTION_HTML. 
2. You might need to update the EMAIL_ADDRESSES_COLUMN variable to point to the right column in your sheet.
3. Enter a couple addresses in the Sheet and test the meeting creation. Go check your calendar for a meeting tomorrow.

## Send Invites
You can send the invites as often as you want by manually triggering them or by scheduling them to send.

1. Send invites manually click QM Quoffee > Send Invitations in your spreadsheet’s menu.
1. Or you can use a Time-Driven trigger.
   1. Open your G Sheet.
   2. Open the script editor by clicking **Tools > Script Editor**.
   3. Click **Edit > Current Project’s Triggers**.
   4. Create your Time-Driven trigger. (We have ours run once a week.)

## Unenroll
If somebody wants to unenroll, then just delete their email from the spreadsheet.
