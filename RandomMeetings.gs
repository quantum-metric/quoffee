// This can be a shared calendar and you invite the administrators of this remote meetup.
const CALENDAR_NAME = "Quoffee Calendar";

// Event name that pepole will see on their calendar
const MEETING_EVENT_SUMMARY_TITLE = "Quoffee";

// Event description in the body of the meeting
let MEETING_DESCRIPTION_HTML = "";
MEETING_DESCRIPTION_HTML = MEETING_DESCRIPTION_HTML.concat("<p><b>ATTENTION: </b>You can <b>reschedule this event yourself</b> if this set time doesn\'t work for all attendees.</p>");
MEETING_DESCRIPTION_HTML = MEETING_DESCRIPTION_HTML.concat("<p>To reschedule, use Google Calendar's <b>'Find a Time'</b> feature to pick a better meeting time. \
Video tutorial here: <a href=\"https://youtu.be/h26iGzjCcJ8?t=25\">https://youtu.be/h26iGzjCcJ8?t=25</a></p>");
MEETING_DESCRIPTION_HTML = MEETING_DESCRIPTION_HTML.concat("<p>'Propose A New Time' won't work because your paired person isn't the Organizer of the meeting. You have to use the 'Find a Time' feature and manually move the meeting. Sorry!</p>");


// The entire column for your email addresses 
const EMAIL_ADDRESSES_COLUMN = "B:B";

// Set to false if you don't have a header row
const HAS_HEADER = true;


/************************************************************************
 *
 * Scheduling
 *
 ************************************************************************
 */

// This custom trigger will run every 2 weeks. Start it from the "QM Quoffee" menu. 
// https://stackoverflow.com/a/32829305
function createBiWeeklyTrigger() {
  // Trigger every other Monday at 06:00.
  ScriptApp.newTrigger('sendMeetings')
      .timeBased()
      .everyWeeks(3)
      .onWeekDay(ScriptApp.WeekDay.MONDAY)
      .atHour(6)
      .create();
}


/************************************************************************
 *
 * Toolbar Menu
 *
 ************************************************************************
 */
// Creates a menu in your Sheets menubar
function onOpen() {
  SpreadsheetApp.getUi().createMenu('QM Quoffee')
    .addItem('Send Invitations', 'sendMeetings')
    .addItem('Start Trigger', 'createBiWeeklyTrigger')
    .addToUi();
}

// Main function to kick off the process. Assumes that the emails are in column B and there's a column header.
function sendMeetings() {
  var attendeeArray = getAttendeeList();
  var numShuffle = getRandomInt(2, 8); // Shuffle the attendee list a random number of times
  Logger.log("Number of shuffles: "+numShuffle);

  for (var i=0; i<numShuffle; i++) {
    attendeeArray = shuffle(attendeeArray);
  }
  
  var attendeesGroups = groupAttendees(attendeeArray);
  Logger.log("attendeesGroups: "+JSON.stringify(attendeesGroups));
  
  createEvents(attendeesGroups);
}




/************************************************************************
 *
 * Handle and Setup the Attendees
 *
 ************************************************************************
 */

// Assumes that the emails are in column B and there's a column header.
function getAttendeeList() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  
  var range = ss.getRange(EMAIL_ADDRESSES_COLUMN);
  
  // Compress the 2D array into 1D. Filter out blanks.
  var values = range.getValues()
    .map(function(arr){
      return arr[0];
    }).filter(function(value){
      return !!value;
    });
  
  if (HAS_HEADER) {
    values.shift();
  }
  
  Logger.log("email addresses: "+values);
  
  return values;
}

/**
 * https://stackoverflow.com/a/1527820
 * 
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// https://stackoverflow.com/a/2450976
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

// Create the groups of attendees for each meeting. Handle uneven number of total people.
function groupAttendees(emailArray) {
  var groupsArray = [];
  var length = emailArray.length;
  var oddLength = (length % 2 !== 0);
  var meetingAttendees;
  
  for (var i=0; i<length; i+=2) {
    meetingAttendees = [];
    
    meetingAttendees.push({email: emailArray[i]});
    meetingAttendees.push({email: emailArray[i+1]});
    
    // If we have an odd length array AND are 3 away from then end, then group those 3 together
    if (oddLength && i == length-3) {
      Logger.log("odd at the end");
      meetingAttendees.push({email: emailArray[i+2]});
      
      // Move i to the end so we can get out of this loop
      i = length;
    }
      
    groupsArray.push(meetingAttendees);
  }
  
  Logger.log("groupsArray: "+JSON.stringify(groupsArray));
  return groupsArray;
}




/************************************************************************
 *
 * Calendar Event Creation
 *
 ************************************************************************
 */

/**
 * Loop through the meeting groups and create an event for each
 * meetingAttendeeArray: [{email:<value1>}, {email:<value2>}]
 */
function createEvents(meetingsAttendeesArray) {
  meetingsAttendeesArray.forEach(function(row, index) {
    createEvent(row);
  });
}

/**
 * Create the event. Attendes are an array of objects with at least email specified in each element, {email:<value>}
 * attendeesArray: [{email:<value1>}, {email:<value2>}]
 */
function createEvent(attendeesArray) {
  Logger.log("attendeesArray: "+ JSON.stringify(attendeesArray));
  
  
  // https://developers.google.com/calendar/v3/reference/events
  // The client-generated unique ID for this request. Clients should regenerate this ID for every new request. If an ID provided is the same as for the previous request, the request is ignored.
  var requestId = makeid_(3)+"-"+makeid_(3)+"-"+makeid_(3);
  Logger.log("requestId: "+requestId);
  
  
  // Get the shared calendar
  var calendars = CalendarApp.getCalendarsByName(CALENDAR_NAME);
  if (calendars.length == 0) {
    throw new Error(CALENDAR_NAME+" not found.");
  }
  
  var calendar = calendars[0].getId();
  
  // https://developers.google.com/apps-script/advanced/calendar#creating_events
  // https://stackoverflow.com/questions/50892845/how-do-i-use-google-apps-script-to-create-a-google-calendar-event-with-hangouts
  var calendarId = calendar;
  var start = getRelativeDate(1, 10, 0); // TODO: This could be improved to find a mutually open time, but this works for now.
  var end = getRelativeDate(1, 10, 20);
  var event = {
    summary: MEETING_EVENT_SUMMARY_TITLE,
    description: MEETING_DESCRIPTION_HTML,
    start: {
      dateTime: start.toISOString(),
      timeZone: 'America/Denver'
    },
    end: {
      dateTime: end.toISOString(),
      timeZone: 'America/Denver'
    },
    attendees: attendeesArray,
    guestsCanModify: true,
    colorId: 10,
    conferenceData: {
      createRequest: {
        conferenceSolutionKey: {
          type: "hangoutsMeet"
        },
        requestId: requestId
      }
    }
  };
  
  event = Calendar.Events.insert(event, calendarId, {"conferenceDataVersion": 1, sendUpdates: "all" });
  Logger.log('Event ID: ' + event.id);
  
  // Throw in a sleep to avoid G Suite throttling
  Utilities.sleep(500);
}

// https://stackoverflow.com/a/1349426
function makeid_(length) {
   var result           = '';
   var characters       = 'abcdefghijklmnopqrstuvwxyz';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

/**
 * Helper function to get a new Date object relative to the current date.
 * @param {number} daysOffset The number of days in the future for the new date.
 * @param {number} hour The hour of the day for the new date, in the time zone
 *     of the script.
 * @return {Date} The new date.
 */
function getRelativeDate(daysOffset, hour, minutes) {
  var date = new Date();
  date.setDate(date.getDate() + daysOffset);
  date.setHours(hour);
  date.setMinutes(minutes || 0);
  date.setSeconds(0);
  date.setMilliseconds(0);
  return date;
}

/**
 * Given an array of attendees, find the next available free time for all attendees.
 * attendeesArray: [{email:<value1>}, {email:<value2>}]
 * https://dzone.com/articles/finding-a-time-to-meet-via-the-google-calendar-api
 * https://developers.google.com/calendar/v3/reference/freebusy/query
 */
//function getNextFreeTime(attendeesArray) {}
